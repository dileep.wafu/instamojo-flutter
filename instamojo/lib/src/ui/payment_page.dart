import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  String testApiKey = 'test_a0d3653a8764c6e5ba21a31fc25';
  String testPrivateAuthToken = 'test_566886506a6ff884d53aa8e0bde';
  String testPrivateSalt = 'e99e89ebb059445c9f0123791c91035d';

  Map<String, String>  headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json",
        "X-Api-Key": 'test_a0d3653a8764c6e5ba21a31fc25',
        "X-Auth-Token": 'test_566886506a6ff884d53aa8e0bde'
      };

  String kAndroidUserAgent =
      "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36";
  Future createRequest() async {
    Map<String, String> body = {
      "amount": "100", //amount to be paid
      "purpose": "Advertising",
      "buyer_name": 'Dileep',
      "email": 'Dileepxdn@gmail.com',
      "phone": '6304502305',
      "allow_repeated_payments": "true",
      "send_email": "false",
      "send_sms": "false",
      "redirect_url": "http://www.example.com/redirect/",
      //Where to redirect after a successful payment.
      "webhook": "http://www.example.com/webhook/",
    };
    //First we have to create a Payment_Request.
    //then we'll take the response of our request.
    var resp = await http.post(
        Uri.encodeFull("https://test.instamojo.com/api/1.1/payment-requests/"),
        headers: headers,
        body: body);
    if (json.decode(resp.body)['success'] == true) {
      _showSnackbar(
          json.decode(resp.body)['message'].toString()); // for testing purpose.
      //If request is successful take the longurl.
      String selectedUrl =
          json.decode(resp.body)["payment_request"]['longurl'].toString() +
              "?embed=form";
      flutterWebviewPlugin.close();
      //Let's open the url in webview.
      flutterWebviewPlugin.launch(selectedUrl,
          rect: new Rect.fromLTRB(
              5.0,
              MediaQuery.of(context).size.height / 7,
              MediaQuery.of(context).size.width - 5.0,
              7 * MediaQuery.of(context).size.height / 7),
          userAgent: kAndroidUserAgent);
    } else {
      _showSnackbar(json.decode(resp.body)['message'].toString());
      //If something is wrong with the data we provided to
      //create the Payment_Request. For Example, the email is in incorrect format, the payment_Request creation will fail.
    }
  }

  _showSnackbar(String response) {
    print('@@@@@@@@@ Show snackbar :: $response');
  }

  _checkPaymentStatus(String id) async {
    String url = "https://www.instamojo.com/api/1.1/payments/$id/"; 
    var response = await http.get(url,headers: headers);
// var response = await http.get(Uri.encodeFull("https://www.instamojo.com/api/1.1/payments/$id/"),+
// headers: {
// "Accept": "application/json",
// "Content-Type": "application/x-www-form-urlencoded",
// "X-Api-Key": "Your-api-key",
// "X-Auth-Token": "Your-auth-token"
// });
var realResponse = json.decode(response.body);
print("REAL RESPONSE BOR @@@@@@@@@@@@@$realResponse");
if (realResponse['success'] == true) {
if (realResponse["payment"]['status'] == 'Credit') {
//payment is successful.
} else {
//payment failed or pending.
}
} else {
print("PAYMENT STATUS FAILED @@@@@@@@@@@@@@@@@@@@");
}

    print("@@@ RESPONSE ID ::: $id");
  }

  @override
  void initState() {
    super.initState();
    // flutterWebviewPlugin.close();
    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      print("@PATYMENT :: $url");
      if (mounted) {
        if (url.contains('http://www.example.com/redirect')) {
          Uri uri = Uri.parse(url);
//Take the payment_id parameter of the url.
          String paymentRequestId = uri.queryParameters['payment_id'];
//calling this method to check payment status
          _checkPaymentStatus(paymentRequestId);
        }
      }
    });

  createRequest();

  }

  @override
  void dispose() {
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: 'http://example.com',
      appBar: AppBar(
        title: Text("PAY"),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.green,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
      initialChild: Center(
        child: CircularProgressIndicator(), // is it working ??
      ),
    );
  }
}
