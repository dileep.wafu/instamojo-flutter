import 'package:flutter/material.dart';
import 'package:instamojo/src/ui/payment_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


@override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Instamojo Payments'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('make payment'),
          color: Colors.greenAccent,
          onPressed: (){
            print("Launching");
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PaymentPage()),);
            

          }
        ),
      ),
      
    );
  }
}
